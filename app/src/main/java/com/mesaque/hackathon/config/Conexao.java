package com.mesaque.hackathon.config;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mesaque.hackathon.model.Professor;

public class Conexao {
    private static FirebaseAuth usuarioLog;
    private static DatabaseReference usuario;
    private static DatabaseReference reference;
    private static StorageReference referenciaStorage;

    public Conexao() {
        usuarioLog = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference();
    }

    public static String getUsuarioId(){
        FirebaseAuth autenticacao = getUsuarioLog();
        return autenticacao.getCurrentUser().getUid();
    }

    public static FirebaseAuth getUsuarioLog() {
        if(usuarioLog == null){
           usuarioLog = FirebaseAuth.getInstance();
        }
        return usuarioLog;
    }

    public static DatabaseReference getReference() {
        if(reference == null){
            reference = FirebaseDatabase.getInstance().getReference();
        }
        return reference;
    }

    public static DatabaseReference getUsuario() {
        if (usuario == null){
            usuario = getReference().child("usuarios");
        }
        return usuario;
    }

    public static StorageReference getReferenciaStorage() {
        if(referenciaStorage == null){
            referenciaStorage = FirebaseStorage.getInstance().getReference();
        }
        return referenciaStorage;
    }
    public static Professor getDadosUserLogin (){
        FirebaseUser firebaseUser = getUsuarioAtual();
        Professor user = new Professor();
        user.setIdUsuario( firebaseUser.getUid());
        return user;
    }
    public static FirebaseUser getUsuarioAtual (){
        FirebaseAuth usuario = getUsuarioLog();
        return usuario.getCurrentUser();
    }
}
