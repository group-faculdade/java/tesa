package com.mesaque.hackathon.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mesaque.hackathon.model.Professor;
import com.mesaque.tesa.R;

import java.util.List;

public class AdapterProfessor extends RecyclerView.Adapter<AdapterProfessor.MyViewHolder> {
    private List<Professor> professors;
    private Context context;

    public AdapterProfessor(List<Professor> professors, Context context) {
        this.professors = professors;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View item = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_professor, viewGroup, false);
        return new MyViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Professor professor = professors.get(i);
        myViewHolder.nomeProfessor.setText(professor.getNome());
        myViewHolder.disciplina.setText(professor.getDisciplina());
        myViewHolder.cidade.setText(professor.getCidade());
        myViewHolder.media.setText(String.format("%.1f", professor.getMedia()));
        myViewHolder.preco.setText(professor.getPreco());
    }

    @Override
    public int getItemCount() {
        return professors.size();
    }

    public class MyViewHolder extends  RecyclerView.ViewHolder{
        TextView nomeProfessor;
        TextView preco;
        TextView disciplina;
        TextView cidade;
        TextView media;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            nomeProfessor = itemView.findViewById(R.id.professorList);
            preco = itemView.findViewById(R.id.precoLista);
            disciplina = itemView.findViewById(R.id.lblDisLis);
            cidade = itemView.findViewById(R.id.lblRuaLis);
            media = itemView.findViewById(R.id.lblMedia);
        }
    }
}
