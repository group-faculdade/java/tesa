package com.mesaque.hackathon.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mesaque.hackathon.model.Professor;
import com.mesaque.tesa.R;

import java.util.List;

public class AdapterProfessoresProfessor extends RecyclerView.Adapter<AdapterProfessoresProfessor.MyViewHolder> {
    private List<Professor> professors;
    private Context context;

    public AdapterProfessoresProfessor(List<Professor> professors, Context context) {
        this.professors = professors;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View item = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_professores_professor, viewGroup, false);
        return new MyViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Professor professor = professors.get(i);
        myViewHolder.lblPrecoList.setText(professor.getPreco());
        myViewHolder.lblDisciplinaList.setText(professor.getDisciplina());
        myViewHolder.lblCidadeList.setText(String.format("%s",professor.getCidade()));
    }

    @Override
    public int getItemCount() {
        return professors.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView lblPrecoList, lblCidadeList, lblDisciplinaList;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            lblCidadeList = itemView.findViewById(R.id.cidadeLista);
            lblDisciplinaList = itemView.findViewById(R.id.disciplinaLbl);
            lblPrecoList = itemView.findViewById(R.id.lblPrecoProf);
        }
    }
}
