package com.mesaque.hackathon.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mesaque.tesa.R;
import com.mesaque.hackathon.model.Agendamento;

import java.util.List;

public class AdapterReservasAluno extends RecyclerView.Adapter<AdapterReservasAluno.MyViewHolder> {
    private List<Agendamento> agendamentos;
    private Context context;
    private boolean isTeacher;

    public AdapterReservasAluno(List<Agendamento> agendamentos, Context context, boolean isTeacher) {
        this.agendamentos = agendamentos;
        this.context = context;
        this.isTeacher = isTeacher;
    }

    @NonNull
    @Override
    public AdapterReservasAluno.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View item = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_reserva_aluno, viewGroup, false);
        return new MyViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterReservasAluno.MyViewHolder myViewHolder, int i) {
        Agendamento agendamento = agendamentos.get(i);
        if(agendamento.isAccept()){
            myViewHolder.viewStatus.setBackgroundColor(0xFF4CAF50);
        }else{
            myViewHolder.viewStatus.setBackgroundColor(0xFFF44336);
        }

        if(isTeacher){
            myViewHolder.lblNomeProfList.setText(agendamento.getNomeAluno());
            myViewHolder.lblDiscList.setText(agendamento.getEmailAluno()); //MUDAR PARA DISCIPLINA DEPOIS
        }else{
            myViewHolder.lblNomeProfList.setText(agendamento.getNomeProfessor());
            myViewHolder.lblDiscList.setText(agendamento.getEmailProfessor()); //MUDAR PARA DISCIPLINA DEPOIS
        }
    }

    @Override
    public int getItemCount() {
        return agendamentos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        View viewStatus;
        TextView lblNomeProfList, lblDiscList;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            lblNomeProfList = itemView.findViewById(R.id.lblProfessorList);
            lblDiscList = itemView.findViewById(R.id.lblPreco);
            viewStatus = itemView.findViewById(R.id.viewStatus);
        }
    }
}
