package com.mesaque.hackathon.model;

import com.google.firebase.database.DatabaseReference;
import com.mesaque.hackathon.config.Conexao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Agendamento implements Serializable {
    private String data;
    private String key;
    private String hora;
    private String idAluno;
    private String idProfessor;
    private boolean accept;
    private String nomeAluno;
    private String emailAluno;
    private String emailProfessor;
    private String nomeProfessor;
    private String telAluno;
    private String telProfessor;
    private String ger;
    private boolean avaliado;

    public Agendamento(){
    }


    public void atualizar(){
        DatabaseReference firebaseref = Conexao.getReference();
        DatabaseReference usuarioRef = firebaseref.child("agendamentoAluno").child( getIdAluno() ).child(getKey());
        DatabaseReference empRef = firebaseref.child("agendamentoProfessor").child(getIdProfessor()).child( getKey());
        Map<String, Object> valorStatus = converterMap();
        usuarioRef.updateChildren( valorStatus );
        empRef.updateChildren( valorStatus );
    }
    public Map<String, Object> converterMap(){
        HashMap<String, Object> usuarioMap = new HashMap<>();
        usuarioMap.put("accept", isAccept());
        usuarioMap.put("key", getKey());
        usuarioMap.put("avaliado", isAvaliado());
        return usuarioMap;
    }


    public String gerarPushKey(){
        DatabaseReference referencia = Conexao.getReference();
        String chave = referencia.push().getKey();
        return chave;
    }

    public void agendarProfessor(){
        setKey(ger);
        DatabaseReference reference = Conexao.getReference();
        DatabaseReference usuarioRef = reference
                .child("agendamentoProfessor")
                .child( getIdProfessor() )
                .child( getKey() );
        usuarioRef.setValue(this);
    }

    public void agendarAluno(){
        ger = gerarPushKey();
        setKey(ger);
        DatabaseReference reference = Conexao.getReference();
        DatabaseReference usuarioRef = reference
                .child("agendamentoAluno")
                .child( getIdAluno() )
                .child( getKey() );
        usuarioRef.setValue(this);
    }

    public boolean isAvaliado() {
        return avaliado;
    }

    public void setAvaliado(boolean avaliado) {
        this.avaliado = avaliado;
    }

    public String getNomeAluno() {
        return nomeAluno;
    }

    public void setNomeAluno(String nomeAluno) {
        this.nomeAluno = nomeAluno;
    }

    public String getEmailAluno() {
        return emailAluno;
    }

    public void setEmailAluno(String emailAluno) {
        this.emailAluno = emailAluno;
    }

    public String getEmailProfessor() {
        return emailProfessor;
    }

    public void setEmailProfessor(String emailProfessor) {
        this.emailProfessor = emailProfessor;
    }

    public String getNomeProfessor() {
        return nomeProfessor;
    }

    public void setNomeProfessor(String nomeProfessor) {
        this.nomeProfessor = nomeProfessor;
    }

    public String getTelAluno() {
        return telAluno;
    }

    public void setTelAluno(String telAluno) {
        this.telAluno = telAluno;
    }

    public String getTelProfessor() {
        return telProfessor;
    }

    public void setTelProfessor(String telProfessor) {
        this.telProfessor = telProfessor;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getIdAluno() {
        return idAluno;
    }

    public void setIdAluno(String idAluno) {
        this.idAluno = idAluno;
    }

    public String getIdProfessor() {
        return idProfessor;
    }

    public void setIdProfessor(String idProfessor) {
        this.idProfessor = idProfessor;
    }

    public boolean isAccept() {
        return accept;
    }

    public void setAccept(boolean accept) {
        this.accept = accept;
    }
}
