package com.mesaque.hackathon.model;

import com.google.firebase.database.DatabaseReference;
import com.mesaque.hackathon.config.Conexao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Usuario implements Serializable {
    private String idUsuario;
    private String nome;
    private String sobrenome;
    private String cidade;
    private String email;
    private String telefone;
    private String tipo_user;

    public void salvar(){
        DatabaseReference firebaseRef = Conexao.getReference();
        DatabaseReference usuarioRef = firebaseRef
                .child("usuarios")
                .child( getIdUsuario() );
        usuarioRef.setValue(this);
    }


    public void atualizarDados(){
        DatabaseReference firebaseref = Conexao.getReference();
        DatabaseReference usuarioRef = firebaseref.child("usuarios").child(getIdUsuario());
        Map<String, Object> valorStatus = converterMap2();
        usuarioRef.updateChildren( valorStatus );
    }
    public Map<String, Object> converterMap2(){
        HashMap<String, Object> usuarioMap = new HashMap<>();
        usuarioMap.put("nome", getNome());
        usuarioMap.put("sobrenome", getSobrenome());
        usuarioMap.put("cidade", getCidade());
        usuarioMap.put("email", getEmail());
        usuarioMap.put("telefone", getTelefone());
        usuarioMap.put("idUsuario", getIdUsuario());
        usuarioMap.put("tipo_user", getTipo_user());
        return usuarioMap;
    }





    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTipo_user() {
        return tipo_user;
    }

    public void setTipo_user(String tipo_user) {
        this.tipo_user = tipo_user;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }
}
