package com.mesaque.hackathon.model;

import com.google.firebase.database.DatabaseReference;
import com.mesaque.hackathon.config.Conexao;

import java.io.Serializable;

public class Avaliacao implements Serializable {
    private float media;
    private int quantidade;
    private String key;
    private String ger;

    public void avaliarAluno(String idAluno){
        ger = gerarPushKey();
        setKey(ger);
        DatabaseReference reference = Conexao.getReference();
        DatabaseReference usuarioRef = reference
                .child("agendamentoAluno")
                .child( idAluno )
                .child("avaliacao");
        usuarioRef.setValue(this);
    }

    public void avaliarProfessor(String idProfessor){
        ger = gerarPushKey();
        setKey(ger);
        DatabaseReference reference = Conexao.getReference();
        DatabaseReference usuarioRef = reference
                .child("agendamentoProfessor")
                .child( idProfessor )
                .child("avaliacao");
        usuarioRef.setValue(this);
    }

    public String gerarPushKey(){
        DatabaseReference referencia = Conexao.getReference();
        String chave = referencia.push().getKey();
        return chave;
    }

    public void calcularMedia(float avaliacao){
        float me = getMedia();
        int quan = getQuantidade();
        float novaMedia;
        novaMedia = ((me*quan)+avaliacao)/quan+1;
        setMedia(novaMedia);
    }

    public float getMedia() {
        return media;
    }

    public void setMedia(float media) {
        this.media = media;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
