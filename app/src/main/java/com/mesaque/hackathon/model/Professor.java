package com.mesaque.hackathon.model;

import com.google.firebase.database.DatabaseReference;
import com.mesaque.hackathon.config.Conexao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Professor extends Usuario implements Serializable {
    private String titulo;
    private String preco;
    private String descricao;
    private String disciplina;
    private boolean accept;
    private double media;
    private int totalAvaliacoes;

    public void atualizar(){
        DatabaseReference firebaseref = Conexao.getReference();
        DatabaseReference usuarioRef = firebaseref.child("usuarios").child(getIdUsuario());
        Map<String, Object> valorStatus = converterMap();
        usuarioRef.updateChildren( valorStatus );
    }
    public Map<String, Object> converterMap(){
        HashMap<String, Object> usuarioMap = new HashMap<>();
        usuarioMap.put("media", getMedia());
        usuarioMap.put("totalAvaliacoes", getTotalAvaliacoes());
        usuarioMap.put("idUsuario", getIdUsuario());
        return usuarioMap;
    }

    @Override
    public void atualizarDados(){
        DatabaseReference firebaseref = Conexao.getReference();
        DatabaseReference usuarioRef = firebaseref.child("usuarios").child(getIdUsuario());
        Map<String, Object> valorStatus = converterMap2();
        usuarioRef.updateChildren( valorStatus );
    }

    @Override
    public Map<String, Object> converterMap2(){
        HashMap<String, Object> usuarioMap = new HashMap<>();
        usuarioMap.put("nome", getNome());
        usuarioMap.put("sobrenome", getSobrenome());
        usuarioMap.put("cidade", getCidade());
        usuarioMap.put("email", getEmail());
        usuarioMap.put("telefone", getTelefone());
        usuarioMap.put("idUsuario", getIdUsuario());
        usuarioMap.put("disciplina", getDisciplina());
        usuarioMap.put("media", getMedia());
        usuarioMap.put("preco", getPreco());
        usuarioMap.put("tipo_user", getTipo_user());
        usuarioMap.put("totalAvaliacoes", getTotalAvaliacoes());
        usuarioMap.put("accept", isAccept());
        return usuarioMap;
    }



    public boolean isAccept() {
        return accept;
    }

    public void setAccept(boolean accept) {
        this.accept = accept;
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getMedia() {
        return media;
    }

    public void setMedia(double media) {
        this.media = media;
    }

    public int getTotalAvaliacoes() {
        return totalAvaliacoes;
    }

    public void setTotalAvaliacoes(int totalAvaliacoes) {
        this.totalAvaliacoes = totalAvaliacoes;
    }
}
