package com.mesaque.hackathon.fragments.aluno;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mesaque.hackathon.adapter.AdapterReservasAluno;
import com.mesaque.hackathon.config.Conexao;
import com.mesaque.hackathon.helper.RecyclerItemClickListener;
import com.mesaque.hackathon.model.Agendamento;
import com.mesaque.hackathon.model.Professor;
import com.mesaque.hackathon.model.Usuario;
import com.mesaque.tesa.R;
import com.mesaque.hackathon.activity.AdapterAvaliacaoProfessores;
import com.mesaque.hackathon.activity.Informacoes;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class InicioFragment extends Fragment {
    public TextView ola;
    private DatabaseReference f = Conexao.getReference();
    private String idLogado;
    private RecyclerView recyclerView;
    private AdapterReservasAluno adapterReservasAluno;
    private List<Agendamento> agendamentos = new ArrayList<>();
    public String nomeUser;
    //private Professor professor;

    public InicioFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_inicio, container, false);

        inicializarComponentes(view);
       // recuperarDados();
        isProfessor(view);

        recuperarDados();

       // ola.setText(String.format("Ola %s", nomeUser));

        return view;

    }

    public void inicializarComponentes(View view){
        ola= view.findViewById(R.id.txtOla);
        recyclerView = view.findViewById(R.id.recyclerListPedidosAluno);
    }

    public void recuperarDados(){
        DatabaseReference reference = f.child("usuarios").child(idLogado);
        reference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildren().equals("Aluno")){
                    System.out.println("Você é aluno" );
                    Usuario u = dataSnapshot.getValue(Usuario.class);
                    ola.setText("Olá, " + u.getNome());
                }else{
                    Professor p = dataSnapshot.getValue(Professor.class);
                    System.out.println("Você é professor" );
                    ola.setText("Olá, " + p.getNome());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public void isProfessor(final View view){
        idLogado = Conexao.getUsuarioId();

        DatabaseReference databaseReference = f.child("agendamentoProfessor").child(idLogado);
        Query query = databaseReference.orderByChild("idProfessor").equalTo(idLogado);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()!=null){
                    //IS TEACHER

                    recuperarReservasProfessor();
                    recyclerView = view.findViewById(R.id.recyclerListPedidosAluno);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    recyclerView.setHasFixedSize(true);
                    adapterReservasAluno = new AdapterReservasAluno(agendamentos, getContext(), true);
                    recyclerView.setAdapter(adapterReservasAluno);
                    recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recyclerView,
                            new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                   /* Agendamento agendamento = agendamentos.get(position);
                                    Avaliacao avaliacao = new Avaliacao();

                                    avaliacao.calcularMedia((float) 4.5);
                                    avaliacao.avaliarProfessor(agendamento.getIdProfessor());*/
                                   Agendamento agendamento = agendamentos.get(position);
                                   Intent intent = new Intent(getContext(), Informacoes.class);
                                   intent.putExtra("agendamento", agendamento);
                                   startActivity(intent);

                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    final Agendamento agendamento = agendamentos.get(position);
                                   /* if(agendamento.isAccept()){

                                        AlertDialog.Builder alerta = new AlertDialog.Builder(getContext());
                                        alerta.setTitle("Cancelar Aula");
                                        alerta.setMessage("Deseja cancelar a aula?");
                                        alerta.setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                agendamento.setAccept(false);
                                                agendamento.atualizar();
                                                Toast.makeText(getContext()
                                                        ,"Aula cancelada!!",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });

                                        alerta.setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Toast.makeText(getContext()
                                                        ,"Aula não cancelada!!",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        alerta.create();
                                        alerta.show();

                                        //getActivity().onBackPressed();
                                       // startActivity(new Intent(getContext(), InicioFragment.class));

                                    }*/
                                   if(!agendamento.isAccept()){
                                        AlertDialog.Builder alerta = new AlertDialog.Builder(getContext());
                                        alerta.setTitle("Confirmar Aula");
                                        alerta.setMessage("Deseja confirmar a aula?");

                                        alerta.setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                agendamento.setAccept(true);
                                                agendamento.atualizar();
                                                Toast.makeText(getContext()
                                                        ,"Aula Confirmada!!",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });

                                        alerta.setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Toast.makeText(getContext()
                                                        ,"Aula não confirmada!!",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        alerta.create();
                                        alerta.show();

                                        //getFragmentManager().popBackStack();
                                        //startActivity(new Intent(getContext(), InicioFragment.class));
                                    }

                                }

                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                }
                            }));
                }else{
                    //NOT IS A TEACHER
                    recuperarReservasAluno();
                    recyclerView = view.findViewById(R.id.recyclerListPedidosAluno);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    recyclerView.setHasFixedSize(true);
                    adapterReservasAluno = new AdapterReservasAluno(agendamentos, getContext(), false);
                    recyclerView.setAdapter(adapterReservasAluno);
                    recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            final Agendamento agendamento = agendamentos.get(position);
                            if(agendamento.isAccept()){
                                if(!agendamento.isAvaliado()){
                                    Intent i = new Intent(getContext(), AdapterAvaliacaoProfessores.class);
                                    i.putExtra("agendamento", agendamento);
                                    startActivity(i);
                                }else{
                                    Toast.makeText(getContext(),"Você já avaliou essa aula", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(getContext(),"Aula não confirmada", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onLongItemClick(View view, int position) {

                        }

                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        }
                    }));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void recuperarReservasProfessor(){
        DatabaseReference professorRef = f.child("agendamentoProfessor").child(idLogado);
        professorRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                agendamentos.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    //System.out.println(snapshot.child("tipo_user").getValue().equals("Professor"));
                    // if(snapshot.child("tipo_user").getValue().equals("Professor")){
                    agendamentos.add(snapshot.getValue(Agendamento.class));
                    //}
                }
                adapterReservasAluno.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



    public void recuperarReservasAluno(){
        DatabaseReference professorRef = f.child("agendamentoAluno").child(idLogado);
        professorRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                agendamentos.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    //System.out.println(snapshot.child("tipo_user").getValue().equals("Professor"));
                    // if(snapshot.child("tipo_user").getValue().equals("Professor")){
                    agendamentos.add(snapshot.getValue(Agendamento.class));
                    //}
                }
                adapterReservasAluno.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }






}
