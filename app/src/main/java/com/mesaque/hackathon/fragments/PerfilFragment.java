package com.mesaque.hackathon.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.mesaque.hackathon.config.Conexao;
import com.mesaque.hackathon.model.Professor;
import com.mesaque.hackathon.model.Usuario;
import com.mesaque.tesa.R;
import com.mesaque.hackathon.activity.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class PerfilFragment extends Fragment {
    private EditText editNome, editSobrenome, editEmail, editCidade, editTelefone;
    private DatabaseReference firebaseRef;
    private String idUserLogin;
    private Button btnAlteracoes;
    private Professor userLogin;
    FirebaseAuth user = Conexao.getUsuarioLog();
    ImageView buttonSair;
    public PerfilFragment() {
        // Required empty public constructor

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_perfil, container, false);
        // Inflate the layout for this fragment

        editNome = view.findViewById(R.id.editNome);
        editSobrenome = view.findViewById(R.id.editSobrenome);
        editEmail = view.findViewById(R.id.editEmail);
        editCidade = view.findViewById(R.id.editCidade);
        editTelefone = view.findViewById(R.id.editTelefone);
        btnAlteracoes = view.findViewById(R.id.buttonEditar);
        buttonSair = view.findViewById(R.id.imageView);
        userLogin = Conexao.getDadosUserLogin();

        idUserLogin = Conexao.getUsuarioId();
        firebaseRef = Conexao.getReference();
        recuperarDados();

        btnAlteracoes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userLogin.setNome(editNome.getText().toString());
                userLogin.setSobrenome(editSobrenome.getText().toString());
                userLogin.setEmail(editEmail.getText().toString());
                userLogin.setCidade(editCidade.getText().toString());
                userLogin.setTelefone(editTelefone.getText().toString());
                userLogin.atualizarDados();
            }
        });
        buttonSair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.imageView:
                        user.signOut();
                        startActivity(new Intent(getContext(), MainActivity.class));
                        break;
                }
            }

        });
        return view;
    }
    public void exibirMensagem (String texto){
        Toast.makeText(getContext(), texto, Toast.LENGTH_SHORT)
                .show();
    }
    public void recuperarDados (){
            DatabaseReference usuarioRef = firebaseRef
                    .child("usuarios")
                    .child(idUserLogin);
            usuarioRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if( dataSnapshot.getValue() != null){
                        Usuario user = dataSnapshot.getValue(Usuario.class);

                        editNome.setText(user.getNome());
                        editSobrenome.setText(user.getSobrenome());
                        editCidade.setText(user.getCidade());
                        editEmail.setText(user.getEmail());
                        editTelefone.setText(user.getTelefone());
                        editEmail.setFocusable(false);

                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
    }
}
