package com.mesaque.hackathon.fragments.aluno;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mesaque.hackathon.config.Conexao;
import com.mesaque.hackathon.helper.RecyclerItemClickListener;
import com.mesaque.hackathon.model.Professor;
import com.mesaque.tesa.R;
import com.mesaque.hackathon.activity.DadosSolicitar;
import com.mesaque.hackathon.adapter.AdapterProfessor;
import com.mesaque.hackathon.adapter.AdapterProfessoresProfessor;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PesquisaFragment extends Fragment {
    private RecyclerView recyclerView;
    private List<Professor> professors = new ArrayList<>();
    private AdapterProfessor adapterProfessor;
    private AdapterProfessoresProfessor adapterProfessoresProfessor;
    private String idUsuarioLog;
    private boolean isProf;
    private String id;

    private DatabaseReference reference;


    public PesquisaFragment() {
        // Required empty public constructor
        reference = Conexao.getReference();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pesquisa, container, false);
/*
            recyclerView = view.findViewById(R.id.recyclerAnuncios);

            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            llm.setOrientation(LinearLayoutManager.VERTICAL);

            recyclerView.setLayoutManager(llm);
            recyclerView.setHasFixedSize(true);

            adapterProfessor = new AdapterProfessor(professors, getContext());
            recyclerView.setAdapter(adapterProfessor);
            recuperarProfessores();
            System.out.println("Not is teacher");
*/
        isProfessor(view);



        return view;
    }

    public void isProfessor(final View view){
        idUsuarioLog = Conexao.getUsuarioId();

        DatabaseReference databaseReference = reference.child("usuarios").child(idUsuarioLog).child("tipo_user");
        //Query query = databaseReference.child(idUsuarioLog).orderByChild("tipo_user");
       //System.out.println("Teste:\n"+query);
        System.out.println("LOGIN");
        System.out.println(databaseReference);
        databaseReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                System.out.println("Ola "+dataSnapshot.getValue());
                if(dataSnapshot.getValue().equals("Aluno")){
                    System.out.println("Entrou: "+dataSnapshot.getChildren());
                    recyclerView = view.findViewById(R.id.recyclerAnuncios);

                    LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                    llm.setOrientation(LinearLayoutManager.VERTICAL);

                    recyclerView.setLayoutManager(llm);
                    recyclerView.setHasFixedSize(true);

                    adapterProfessor = new AdapterProfessor(professors, getContext());
                    recyclerView.setAdapter(adapterProfessor);
                    //GET POSITION
                    recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            Professor professor = professors.get(position);
                            Intent i = new Intent(getContext(), DadosSolicitar.class);
                            i.putExtra("professor", professor);
                            /*Bundle params = new Bundle();
                            params.putString("nome", professor.getNome());
                            params.putString("disciplina", professor.getDisciplina());
                            params.putString("idProf", professor.getIdUsuario());
*/
                            //params.putSerializable("professor", professor);
                           // startActivity(new Intent(getContext(), DadosSolicitar.class).putExtras(params));
                            startActivity(i);
                        }

                        @Override
                        public void onLongItemClick(View view, int position) {

                        }

                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        }
                    }));
                    recuperarProfessores();

                }else{

                    recyclerView = view.findViewById(R.id.recyclerAnuncios);

                    LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                    llm.setOrientation(LinearLayoutManager.VERTICAL);

                    recyclerView.setLayoutManager(llm);
                    recyclerView.setHasFixedSize(true);
                    adapterProfessoresProfessor = new AdapterProfessoresProfessor(professors, getContext());
                    //adapterProfessor = new AdapterProfessor(professors, getContext());
                    recyclerView.setAdapter(adapterProfessoresProfessor);
                    recuperarToProfessores();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void recuperarProfessores(){
        DatabaseReference professorRef = reference.child("usuarios");
        Query query = professorRef.orderByChild("tipo_user").equalTo("Professor");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                professors.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    //System.out.println(snapshot.child("tipo_user").getValue().equals("Professor"));
                   // if(snapshot.child("tipo_user").getValue().equals("Professor")){
                    professors.add(snapshot.getValue(Professor.class));
                    //}
                }
                adapterProfessor.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void recuperarToProfessores(){
        DatabaseReference professorRef = reference.child("usuarios");
        Query query = professorRef.orderByChild("tipo_user").equalTo("Professor");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                professors.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    //System.out.println(snapshot.child("tipo_user").getValue().equals("Professor"));
                    // if(snapshot.child("tipo_user").getValue().equals("Professor")){
                    professors.add(snapshot.getValue(Professor.class));
                    //}
                }
                adapterProfessoresProfessor.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
