package com.mesaque.hackathon.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.mesaque.hackathon.config.Conexao;
import com.mesaque.tesa.R;

public class index extends AppCompatActivity {
    public Conexao conexao = new Conexao();
    //private DatabaseReference usuarioData = conexao.getUsuario();
    private FirebaseAuth usuario = conexao.getUsuarioLog();
    private TextView campoEmail;
    private TextView campoSenha;

    private String email;
    private String senha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        campoEmail = findViewById(R.id.txt_Email);
        campoSenha = findViewById(R.id.txt_Senha);

        //Usuario user = new Usuario();
        /*user.setNome("Mesaque");
        usuarioData.push().setValue(user);*/
        //user.setIdUsuario(conexao.ge);
        System.out.println("Teste");
        System.out.println(conexao.getUsuario().toString());

        verificaLogado();

    }

    public void entrar(final View view){
        if(!(campoSenha.getText().toString().equals("")) && !(campoEmail.getText().toString().equals(""))){
            email = campoEmail.getText().toString();
            senha = campoSenha.getText().toString();

            usuario.signInWithEmailAndPassword(email,senha).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        startActivity(new Intent(getApplicationContext(), TelaUsuarioActivity.class));
                        finish();
                    } else{
                        AlertDialog.Builder alerta = new AlertDialog.Builder(index.this);
                        alerta.setTitle("Tente novamente");
                        alerta.setMessage("Email ou senha inválido");
                        alerta.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alerta.create();
                        alerta.show();
                    }
                }
            });

        }else{
            Toast.makeText(getApplicationContext(), "Preencha todos os campos", Toast.LENGTH_LONG).show();
        }
    }

    public void cadastrar(View view){
        startActivity(new Intent(this, Cadastro.class));
    }

    public void mudar(View view){
        startActivity(new Intent(this, TelaUsuarioActivity.class));
    }

    public void verificaLogado(){
        if(usuario.getCurrentUser() != null){
            startActivity(new Intent(getApplicationContext(), TelaUsuarioActivity.class));
            finish();
        }
    }
}
