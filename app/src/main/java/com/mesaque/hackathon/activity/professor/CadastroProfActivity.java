package com.mesaque.hackathon.activity.professor;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.blackcat.currencyedittext.CurrencyEditText;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.mesaque.hackathon.config.Conexao;
import com.mesaque.hackathon.model.Professor;
import com.mesaque.tesa.R;
import com.mesaque.hackathon.activity.TelaUsuarioActivity;

import java.util.Locale;

public class CadastroProfActivity extends AppCompatActivity {
    private Conexao conexao = new Conexao();
    private Professor professores = new Professor();

    private FirebaseAuth usuarioLog = conexao.getUsuarioLog();

    private TextView campoDisci;
    private CurrencyEditText campoPreco;
    private String preco, disci;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_prof);

        campoPreco = findViewById(R.id.txtPreco);
        campoDisci = findViewById(R.id.txtDisci);

        Locale locale = new Locale("pt", "BR");
        campoPreco.setLocale(locale);

    }

    public void cadastrar(View view) {
        Intent recebe = getIntent();
        Bundle params = recebe.getExtras();

        if (params != null) {
            String nome = params.getString("nome");
            String sobrenome = params.getString("sobrenome");
            String cidade = params.getString("cidade");
            String email = params.getString("email");
            String senha = params.getString("senha");
            String telefone = params.getString("telefone");
            preco = campoPreco.getText().toString();
            disci = campoDisci.getText().toString();
            String tip = "Professor";

            professores.setNome(nome);
            professores.setSobrenome(sobrenome);
            professores.setCidade(cidade);
            professores.setEmail(email);
            professores.setTelefone(telefone);
            professores.setDisciplina(disci);
            professores.setPreco(preco);
            professores.setMedia(0);
            professores.setTotalAvaliacoes(0);

            professores.setTipo_user(tip);
           // professores.setTitulo(tit);

            System.out.println(professores.getNome() + "\n" + professores.getTipo_user() + "\n" + professores.getTitulo());

            usuarioLog.createUserWithEmailAndPassword(email, senha).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        professores.setIdUsuario(conexao.getUsuarioId());
                        // usuarioData.push().setValue(professores);
                        professores.salvar();
                        finish();
                        startActivity(new Intent(getApplicationContext(), TelaUsuarioActivity.class));
                    } else {
                        System.out.println("Error");
                    }
                }
            });
        }
    }


}
