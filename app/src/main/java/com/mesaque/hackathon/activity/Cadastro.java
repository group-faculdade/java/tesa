package com.mesaque.hackathon.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.mesaque.hackathon.activity.professor.CadastroProfActivity;
import com.mesaque.hackathon.config.Conexao;
import com.mesaque.hackathon.model.Usuario;
import com.mesaque.tesa.R;
import com.santalu.maskedittext.MaskEditText;

public class Cadastro extends AppCompatActivity {
    private Conexao conexao = new Conexao();
    private Usuario usuario = new Usuario();

    private FirebaseAuth usuarioLog = conexao.getUsuarioLog();

    private TextView campoNome, campoSobrenome, campoCidade, campoEmail, campoSenha;
    private MaskEditText campoTelefone;
    private Switch switchStado;
    private String nome, sobrenome, cidade, email, senha, tipoUser, telefone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        campoNome = findViewById(R.id.txtNome);
        campoSobrenome = findViewById(R.id.txtSobrenome);
        campoCidade = findViewById(R.id.txtCidade);
        campoEmail = findViewById(R.id.txtEmail);
        campoSenha = findViewById(R.id.txtSenha);
        switchStado = findViewById(R.id.switchProfessor);
        campoTelefone = findViewById(R.id.txtTelefone);
    }
    public void cadastrar(View view) {
        if (!(campoSenha.getText().toString().equals("")) && !(campoEmail.getText().toString().equals(""))
                && !(campoCidade.getText().toString().equals("")) && !(campoSobrenome.getText().toString().equals(""))
                && !(campoNome.getText().toString().equals(""))) {

            //PEGA VALORES
            nome = campoNome.getText().toString();
            sobrenome = campoSobrenome.getText().toString();
            cidade = campoCidade.getText().toString();
            email = campoEmail.getText().toString();
            senha = campoSenha.getText().toString();
            telefone = campoTelefone.getText().toString();

            //SETAR OS VALORES DO OBJETO
            usuario.setNome(nome);
            usuario.setSobrenome(sobrenome);
            usuario.setCidade(cidade);
            usuario.setEmail(email);
            usuario.setTelefone(telefone);

            //VERIFICA SE É ALUNO OU PROFESSOR
            if (switchStado.isChecked()) {
                tipoUser = "Professor";
            } else {
                tipoUser = "Aluno";
            }

            //setando o tipo do user
            usuario.setTipo_user(tipoUser);

            //CRIA NO BANCO DE DADOS


            if (tipoUser.equals("Professor")) {
                Bundle params = new Bundle();

                params.putString("nome", nome);
                params.putString("sobrenome", sobrenome);
                params.putString("cidade", cidade);
                params.putString("email", email);
                params.putString("senha", senha);
                params.putString("telefone", telefone);
                startActivity(new Intent(getApplicationContext(), CadastroProfActivity.class).putExtras(params));
            } else {
                usuarioLog.createUserWithEmailAndPassword(email, senha).addOnCompleteListener(Cadastro.this, new OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        System.out.println("Entrou CREATE");
                        if (task.isSuccessful()) {
                            System.out.println("entrou IF");
                            usuario.setIdUsuario(conexao.getUsuarioId());
                            /*usuarioData = conexao.getUsuario();
                            usuarioData.push().setValue(usuario);*/
                            usuario.salvar();
                            startActivity(new Intent(getApplicationContext(), TelaUsuarioActivity.class));
                        } else {
                            String excessao = "";
                            try {
                                throw task.getException();
                            } catch (FirebaseAuthWeakPasswordException e) {
                                excessao = "Digite uma senha mais forte!";
                            } catch (FirebaseAuthInvalidCredentialsException e) {
                                excessao = "Email inválido!";
                            } catch (FirebaseAuthUserCollisionException e) {
                                excessao = "Esta conta já existe!";
                            } catch (Exception e) {
                                excessao = "Erro ao cadastrar o usuário " + e.getMessage();
                                e.printStackTrace();
                            }
                            Toast.makeText(getApplicationContext(), excessao, Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }
        }
    }
}
