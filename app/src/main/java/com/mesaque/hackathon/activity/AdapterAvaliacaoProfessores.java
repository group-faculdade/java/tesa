package com.mesaque.hackathon.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.mesaque.hackathon.config.Conexao;
import com.mesaque.hackathon.fragments.aluno.InicioFragment;
import com.mesaque.hackathon.model.Agendamento;
import com.mesaque.hackathon.model.Professor;
import com.mesaque.tesa.R;

public class AdapterAvaliacaoProfessores extends AppCompatActivity {
    private TextView porcentagem;
    private SeekBar seekPorcentagem;
    private Agendamento agendamento;
    private DatabaseReference f = Conexao.getReference();
    private Button btnAvaliar;
    private Professor professor;


    private double porcentageCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter_avaliacao_professores);
        inicializarComponentes();
        //controlar seekBar
        seekPorcentagem.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                porcentageCount = getConvertedValue(i);
                porcentagem.setText(String.format("%.1f", porcentageCount));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            agendamento = (Agendamento) bundle.get("agendamento");
        }

        recuperarDadosProfessor();

        btnAvaliar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    double antMedia = professor.getMedia();
                    int totalAv = professor.getTotalAvaliacoes();
                    float totalMedia = (float) (antMedia*totalAv);
                    float totalMediaAt = (float) (totalMedia+porcentageCount);
                    float nota = totalMediaAt/(totalAv+1);
                    professor.setMedia(nota);
                    professor.setTotalAvaliacoes(totalAv+1);
                    professor.atualizarDados();

                    agendamento.setAvaliado(true);
                    agendamento.atualizar();

                    Toast.makeText(getApplicationContext()
                            ,"OBRIGADO por me avaliar!",
                            Toast.LENGTH_LONG).show();
                    finish();
                    startActivity(new Intent(getApplicationContext(), InicioFragment.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public void recuperarDadosProfessor(){
        DatabaseReference professorRef = f.child("usuarios").child(agendamento.getIdProfessor());
        professorRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                professor = dataSnapshot.getValue(Professor.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public double getConvertedValue(int intVal){
        double floatVal = 0.0;
        floatVal = .1f * intVal;
        return floatVal;
    }

    public void inicializarComponentes(){
        porcentagem = findViewById(R.id.porcentagem_lbl);
        seekPorcentagem = findViewById(R.id.seekBar);
        btnAvaliar = findViewById(R.id.btnAvaliar);
    }
}
