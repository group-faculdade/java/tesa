package com.mesaque.hackathon.activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.mesaque.hackathon.config.Conexao;
import com.mesaque.hackathon.model.Agendamento;
import com.mesaque.hackathon.model.Usuario;
import com.mesaque.tesa.R;

public class Informacoes extends AppCompatActivity {
    TextView lblNome, lblCidade, lblEmail, lblTelefone, lblData, lblHorario;
    private Agendamento agendamento;
    private DatabaseReference f = Conexao.getReference();
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacoes);

        inicializarComponentes();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            agendamento = (Agendamento) bundle.get("agendamento");

        }

        recuperarUser();

    }

    public void inicializarComponentes(){
        lblCidade = findViewById(R.id.lbl_cidade_info);
        lblNome = findViewById(R.id.lbl_nome_info);
        lblEmail = findViewById(R.id.lbl_email_info);
        lblTelefone = findViewById(R.id.lbl_telefone_info);
        lblTelefone = findViewById(R.id.lbl_telefone_info);
        lblData = findViewById(R.id.lbl_data_info);
        lblHorario = findViewById(R.id.lbl_horario_info);
    }

    public void setText(){
        lblNome.setText(String.format("%s %s", usuario.getNome(), usuario.getSobrenome()));
        lblCidade.setText(String.format("%s", usuario.getCidade()));
        lblEmail.setText(String.format("%s", usuario.getEmail()));
        lblTelefone.setText(String.format("%s", usuario.getTelefone()));
        lblData.setText(String.format("%s", agendamento.getData()));
        lblHorario.setText(String.format("%s", agendamento.getHora()));

    }

    public void recuperarUser(){
        DatabaseReference user = f.child("usuarios").child(agendamento.getIdAluno());
        user.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usuario = dataSnapshot.getValue(Usuario.class);
                setText();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
