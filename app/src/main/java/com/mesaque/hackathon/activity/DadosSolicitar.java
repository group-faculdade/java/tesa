package com.mesaque.hackathon.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.mesaque.hackathon.config.Conexao;
import com.mesaque.hackathon.model.Agendamento;
import com.mesaque.hackathon.model.Professor;
import com.mesaque.hackathon.model.Usuario;
import com.mesaque.tesa.R;

public class DadosSolicitar extends AppCompatActivity implements View.OnClickListener {
    DatabaseReference reference = Conexao.getReference();
    Professor professor;
    Usuario aluno;
    Agendamento agendamento;

    private TextView txtNomeProf, txtDescricaoConfir, txtQtdAvali;
    private CalendarView calendarView;
    private String idProf;
    private String idAluno = Conexao.getUsuarioId();
    private Button btnSolicitar;
    private String data;
    private String hora;
    private ToggleButton buttons[] = new ToggleButton[15];

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados_solicitar);

        //toolbar
        Toolbar toolbar = findViewById(R.id.toolbarPrincipal);
        //toolbar.setTitle("Novo Serviço");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_seta_white_24dp);

        inicializarComponentes();

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int dayOfMonth) {
                int mes = month + 1;
                data = dayOfMonth + "/" + mes + "/" + year;
            }
        });
        recuperarAluno();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            professor = (Professor) bundle.get("professor");
            txtNomeProf.setText(professor.getNome());
            txtDescricaoConfir.setText(String.format("Olá, meu nome é %s %s e pretendo ser seu professor de %s.",
                    professor.getNome(), professor.getSobrenome(), professor.getDisciplina()));
            txtQtdAvali.setText(String.format("%d avaliações", professor.getTotalAvaliacoes()));
        }

        btnSolicitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                agendamento = new Agendamento();
                agendamento.setData(data);
                agendamento.setHora(hora);
                agendamento.setIdProfessor(professor.getIdUsuario());
                agendamento.setNomeProfessor(professor.getNome());
                agendamento.setEmailProfessor(professor.getEmail());

                agendamento.setIdAluno(idAluno);
                agendamento.setEmailAluno(aluno.getEmail());
                agendamento.setNomeAluno(aluno.getNome());

                agendamento.setAccept(false);
                agendamento.setAvaliado(false);

                agendamento.agendarAluno();
                agendamento.agendarProfessor();

                Toast.makeText(DadosSolicitar.this
                        , "Cadastrou",
                        Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), TelaUsuarioActivity.class));
            }
        });
    }

    public void inicializarComponentes() {
        txtDescricaoConfir = findViewById(R.id.txtDescricaoConfir);
        txtNomeProf = findViewById(R.id.txtNomeProf);
        txtQtdAvali = findViewById(R.id.qtdAvaliacoes);
        btnSolicitar = findViewById(R.id.btnSolicitar);
        calendarView = findViewById(R.id.calendarView);


        //INICIALIZANDO OS BOTOES DA HORA (CADA UM REPRESENTA MEIA HORA)
        buttons[0] = findViewById(R.id.toggleButton2); //07:00
        buttons[1] = findViewById(R.id.toggleButton3);
        buttons[2] = findViewById(R.id.toggleButton4);
        buttons[3] = findViewById(R.id.toggleButton5);
        buttons[4] = findViewById(R.id.toggleButton6);
        buttons[5] = findViewById(R.id.toggleButton7);
        buttons[6] = findViewById(R.id.toggleButton8);
        buttons[7] = findViewById(R.id.toggleButton9);
        buttons[8] = findViewById(R.id.toggleButton10);
        buttons[9] = findViewById(R.id.toggleButton11);
        buttons[10] = findViewById(R.id.toggleButton12);
        buttons[11] = findViewById(R.id.toggleButton13);
        buttons[12] = findViewById(R.id.toggleButton14);
        buttons[13] = findViewById(R.id.toggleButton15);
        buttons[14] = findViewById(R.id.toggleButton16);

        for (int i = 0; i < buttons.length; i++) {
            buttons[i].setOnClickListener(this);
        }

    }

    public void recuperarAluno() {
        DatabaseReference professorRef = reference.child("usuarios").child(idAluno);
        professorRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    aluno = dataSnapshot.getValue(Usuario.class);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toggleButton2:
                hora = "07:00";
                for (int i = 0; i < buttons.length; i++) {
                    if (i != 0) {
                        buttons[i].setChecked(false);
                    }
                }
                break;
            case R.id.toggleButton3:
                hora = "08:00";
                for (int i = 0; i < buttons.length; i++) {
                    if (i != 1) {
                        buttons[i].setChecked(false);
                    }
                }
                break;
            case R.id.toggleButton4:
                hora = "09:00";
                for (int i = 0; i < buttons.length; i++) {
                    if (i != 2) {
                        buttons[i].setChecked(false);
                    }
                }
                break;
            case R.id.toggleButton5:
                hora = "10:00";
                for (int i = 0; i < buttons.length; i++) {
                    if (i != 3) {
                        buttons[i].setChecked(false);
                    }
                }
                break;
            case R.id.toggleButton6:
                hora = "11:00";
                for (int i = 0; i < buttons.length; i++) {
                    if (i != 4) {
                        buttons[i].setChecked(false);
                    }
                }
                break;
            case R.id.toggleButton7:
                hora = "13:00";
                for (int i = 0; i < buttons.length; i++) {
                    if (i != 5) {
                        buttons[i].setChecked(false);
                    }
                }
                break;
            case R.id.toggleButton8:
                hora = "14:00";
                for (int i = 0; i < buttons.length; i++) {
                    if (i != 6) {
                        buttons[i].setChecked(false);
                    }
                }
                break;
            case R.id.toggleButton9:
                hora = "15:00";
                for (int i = 0; i < buttons.length; i++) {
                    if (i != 7) {
                        buttons[i].setChecked(false);
                    }
                }
                break;
            case R.id.toggleButton10:
                hora = "16:00";
                for (int i = 0; i < buttons.length; i++) {
                    if (i != 8) {
                        buttons[i].setChecked(false);
                    }
                }
                break;
            case R.id.toggleButton11:
                hora = "17:00";
                for (int i = 0; i < buttons.length; i++) {
                    if (i != 9) {
                        buttons[i].setChecked(false);
                    }
                }
                break;
            case R.id.toggleButton12:
                hora = "18:00";
                for (int i = 0; i < buttons.length; i++) {
                    if (i != 10) {
                        buttons[i].setChecked(false);
                    }
                }
                break;
            case R.id.toggleButton13:
                hora = "19:00";
                for (int i = 0; i < buttons.length; i++) {
                    if (i != 11) {
                        buttons[i].setChecked(false);
                    }
                }
                break;
            case R.id.toggleButton14:
                hora = "20:00";
                for (int i = 0; i < buttons.length; i++) {
                    if (i != 12) {
                        buttons[i].setChecked(false);
                    }
                }
                break;
            case R.id.toggleButton15:
                hora = "21:00";
                for (int i = 0; i < buttons.length; i++) {
                    if (i != 13) {
                        buttons[i].setChecked(false);
                    }
                }
                break;
            case R.id.toggleButton16:
                hora = "22:00";
                for (int i = 0; i < buttons.length; i++) {
                    if (i != 14) {
                        buttons[i].setChecked(false);
                    }
                }
                break;
        }
    }
}
