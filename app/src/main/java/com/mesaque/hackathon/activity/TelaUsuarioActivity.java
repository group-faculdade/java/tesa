package com.mesaque.hackathon.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.mesaque.hackathon.config.Conexao;
import com.mesaque.hackathon.fragments.PerfilFragment;
import com.mesaque.hackathon.fragments.aluno.InicioFragment;
import com.mesaque.hackathon.fragments.aluno.PesquisaFragment;
import com.mesaque.tesa.R;

public class TelaUsuarioActivity extends AppCompatActivity {
    Conexao conexao = new Conexao();
    FirebaseAuth usuario = conexao.getUsuarioLog();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_usuario);
        verificaLogado();

        configuraBottomNavigationView();
        FragmentManager fragmentManager = (FragmentManager) getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = (FragmentTransaction) fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.frameLayout, new InicioFragment()).commit();
    }
    private void configuraBottomNavigationView(){
        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.include);
       // try {
            habilitaNavegacao(bottomNavigationViewEx);
        //}catch (Exception e){
         //   System.out.println("Error");
        //}

    }

    private void habilitaNavegacao(BottomNavigationViewEx viewEx){

        viewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                FragmentManager fragmentManager = (FragmentManager) getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = (FragmentTransaction) fragmentManager.beginTransaction();
                switch (menuItem.getItemId()){
                    case R.id.ic_home:
                        fragmentTransaction.replace(R.id.frameLayout, new InicioFragment()).commit();
                        return true;
                    case R.id.ic_search:
                        fragmentTransaction.replace(R.id.frameLayout, new PesquisaFragment()).commit();
                        return true;
                    case R.id.ic_person:
                        fragmentTransaction.replace(R.id.frameLayout, new PerfilFragment()).commit();
                        return true;
                }
                return false;
            }
        });
    }


    public void verificaLogado(){
        if(usuario.getCurrentUser() == null){
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
    }
}
